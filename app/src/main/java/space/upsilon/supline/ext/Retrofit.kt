package space.upsilon.supline.ext

import io.reactivex.Single
import retrofit2.Response

/**
 * Author: Andrey Khitryy
 * Email: andrey.khitryy@gmail.com
 */
fun <T> Single<Response<T>>.asRetrofitResponse() =
        this.map {
            if (!it.isSuccessful)
                throw java.io.IOException()
            it
        }!!

fun <T> Single<Response<T>>.asRetrofitBody() =
        this.asRetrofitResponse().map { it.body() }!!
package space.upsilon.supline.ext

import android.content.res.Resources

/**
 * Author: Andrey Khitryy
 * Email: andrey.khitryy@gmail.com
 */
fun Int.asDpToPx() = this * Resources.getSystem().displayMetrics.density.toInt()

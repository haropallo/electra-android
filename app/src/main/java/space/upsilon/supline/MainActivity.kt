package space.upsilon.supline

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.ArrayAdapter
import com.applandeo.materialcalendarview.EventDay
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.activity_main.*
import space.upsilon.supline.data.applianceRepository
import space.upsilon.supline.data.remote.api
import space.upsilon.supline.data.remote.entry.Building
import space.upsilon.supline.ext.asRetrofitBody
import space.upsilon.supline.ui.DayActivity
import space.upsilon.supline.ui.common.BaseActivity
import java.util.*


class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        applianceRepository.sync()
        api.getBaseBuildingsComsumptions()
                .observeOn(AndroidSchedulers.mainThread())
                .asRetrofitBody()
                .subscribe({
                    val adapter = ArrayAdapter<Building>(this, android.R.layout.simple_spinner_item, it)
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                    spinner.adapter = adapter
                }, this::processError)
                .bind()

        // 21.01.2016 - Bad day(yesterday)
        // 25.11.2017 - Today
        // 26.11.2017 - TMRW

        calendarView.setEvents(listOf(

                EventDay(Calendar.getInstance().apply {
                    set(Calendar.DAY_OF_YEAR, get(Calendar.DAY_OF_YEAR) - 1)
                }, R.drawable.danger),

                EventDay(Calendar.getInstance().apply {
                    set(Calendar.DAY_OF_YEAR, get(Calendar.DAY_OF_YEAR) - 2)
                }, R.drawable.danger),

                EventDay(Calendar.getInstance().apply {
                    set(Calendar.DAY_OF_YEAR, get(Calendar.DAY_OF_YEAR) - 3)
                }, R.drawable.danger),

                EventDay(Calendar.getInstance().apply {
                    set(Calendar.DAY_OF_YEAR, get(Calendar.DAY_OF_YEAR))
                }, R.drawable.good),
                EventDay(Calendar.getInstance().apply {
                    set(Calendar.DAY_OF_YEAR, get(Calendar.DAY_OF_YEAR) + 1)
                }, R.drawable.good))
        )

        calendarView.setOnDayClickListener {
            startActivity(DayActivity.getStartIntent(
                    this,
                    "${it.calendar.get(Calendar.YEAR)}-${it.calendar.get(Calendar.MONTH) + 1}-${it.calendar.get(Calendar.DAY_OF_MONTH)}",
                    spinner.selectedItem as Building
            ))
        }
    }
}

package space.upsilon.supline.data

import space.upsilon.supline.data.remote.api
import space.upsilon.supline.data.remote.entry.Appliance
import space.upsilon.supline.ext.asRetrofitBody

/**
 * Author: Andrey Khitryy
 * Email: andrey.khitryy@gmail.com
 */

val applianceRepository = ApplianceRepository()

class ApplianceRepository {
    private val appliences = mutableListOf<Appliance>()
    fun getAll() = appliences

    fun sync() = api.getAppliances()
            .asRetrofitBody()
            .subscribe({
                appliences.clear()
                appliences.addAll(it)
            }, {
                it.printStackTrace()
            })

    fun find(id: Long) = appliences.find { it.id == id }!!
}
package space.upsilon.supline.data.remote.entry

/**
 * Author: Andrey Khitryy
 * Email: andrey.khitryy@gmail.com
 */
data class Price(
        val id: Long,
        val value: Float
)
package space.upsilon.supline.data

import space.upsilon.supline.data.remote.entry.Appliance
import java.io.Serializable

/**
 * Author: Andrey Khitryy
 * Email: andrey.khitryy@gmail.com
 */
data class DaySetup(
        val day: String,
        val setups: MutableList<Appliance> = mutableListOf()
) : Serializable
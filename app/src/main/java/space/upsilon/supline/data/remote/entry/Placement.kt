package space.upsilon.supline.data.remote.entry

/**
 * Author: Andrey Khitryy
 * Email: andrey.khitryy@gmail.com
 */
data class Placement(
        val time_segment_id: Int,
        val appliance: Long
)
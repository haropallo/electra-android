package space.upsilon.supline.data.remote.entry

import java.io.Serializable

/**
 * Author: Andrey Khitryy
 * Email: andrey.khitryy@gmail.com
 */
data class Building(
        val name: String,
        val consumptions: List<Float>
) : Serializable {

    override fun toString() = name
}
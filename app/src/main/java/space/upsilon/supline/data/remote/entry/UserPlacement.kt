package space.upsilon.supline.data.remote.entry

import java.io.Serializable

/**
 * Author: Andrey Khitryy
 * Email: andrey.khitryy@gmail.com
 */
data class UserPlacement(
        val day: String,
        var placement: MutableList<Placement> = mutableListOf()
) : Serializable
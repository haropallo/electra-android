package space.upsilon.supline.data.remote

import com.google.gson.GsonBuilder
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import space.upsilon.supline.data.remote.entry.Appliance
import space.upsilon.supline.data.remote.entry.Building
import space.upsilon.supline.data.remote.entry.Price
import java.util.concurrent.TimeUnit

/**
 * Author: Andrey Khitryy
 * Email: andrey.khitryy@gmail.com
 */
interface Api {
    @GET("prices")
    fun getPricesForDay(@Query("date") date: String): Single<Response<List<Price>>>

    @GET("appliances")
    fun getAppliances() : Single<Response<List<Appliance>>>

    @GET("base_consumptions")
    fun getBaseBuildingsComsumptions() : Single<Response<List<Building>>>
}

val api = Retrofit.Builder()
        .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
        .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
        .baseUrl("http://junction2017.herokuapp.com/api/v1/")
        .client(
                OkHttpClient.Builder()
                        .connectTimeout(5, TimeUnit.SECONDS)
                        .readTimeout(5, TimeUnit.SECONDS)
                        .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                        .build()
        ).build().create(Api::class.java)!!
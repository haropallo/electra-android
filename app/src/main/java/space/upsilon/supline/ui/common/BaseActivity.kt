package space.upsilon.supline.ui.common

import android.annotation.SuppressLint
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

/**
 * Author: Andrey Khitryy
 * Email: andrey.khitryy@gmail.com
 */
@SuppressLint("Registered")
open class BaseActivity : AppCompatActivity() {
    private val composite = CompositeDisposable()

    fun Disposable.bind() = composite.add(this)

    fun unbindAll() = composite.clear()

    override fun onDestroy() {
        super.onDestroy()
        composite.clear()
    }

    fun processError(e: Throwable) {
        AlertDialog.Builder(this)
                .setMessage(e.message)
                .create().show()
    }
}
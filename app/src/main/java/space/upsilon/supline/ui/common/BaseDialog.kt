package space.upsilon.supline.ui.common

import android.support.v4.app.DialogFragment
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

/**
 * Author: Andrey Khitryy
 * Email: andrey.khitryy@gmail.com
 */
open class BaseDialog : DialogFragment() {

    private val composite = CompositeDisposable()

    fun Disposable.bind() = composite.add(this)

    fun unbindAll() = composite.clear()

    override fun onDestroyView() {
        super.onDestroyView()
        composite.clear()
    }
}
package space.upsilon.supline.ui

import android.content.Context
import android.graphics.*
import android.support.v4.view.GestureDetectorCompat
import android.support.v7.app.AlertDialog
import android.util.Log
import android.util.SparseArray
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View
import space.upsilon.supline.R
import space.upsilon.supline.data.applianceRepository
import space.upsilon.supline.data.remote.entry.Building
import space.upsilon.supline.data.remote.entry.Price
import space.upsilon.supline.data.remote.entry.UserPlacement
import space.upsilon.supline.ext.asDpToPx


/**
 * Author: Andrey Khitryy
 * Email: andrey.khitryy@gmail.com
 */
class PriceGraphView(c: Context, val building: Building, onSelect: (daytime: Int) -> Unit) : View(c) {

    private val blockPain = Paint().apply {
        style = Paint.Style.FILL
        color = Color.RED
    }

    private val paint = Paint().apply {
        style = Paint.Style.STROKE
        strokeCap = Paint.Cap.ROUND
        color = Color.GREEN
        strokeWidth = 4.asDpToPx().toFloat()
        isAntiAlias = true
    }

    private val daytimeDividerPaint = Paint().apply {
        style = Paint.Style.STROKE
        pathEffect = DashPathEffect(listOf(10.0f, 10.0f).toFloatArray(), 0.0f)
    }

    private val pricePaint = Paint().apply {
        textAlign = Paint.Align.CENTER
        textSize = 18f
    }

    private val bars = 8
    private val path = Path()

    var daytimeMult = 0.0f
    var profitMult = 0.0f

    var prices = listOf<Price>()
    var blocks = SparseArray<MutableList<Float>>()

    private val clickDetector = GestureDetectorCompat(
            context, object : GestureDetector.SimpleOnGestureListener() {
        override fun onSingleTapConfirmed(e: MotionEvent): Boolean {
            val position = (0..bars).firstOrNull { e.x > it * daytimeMult && e.x < (it + 1.0f) * daytimeMult } ?: 0
            onSelect.invoke(position)
            return true
        }

        override fun onLongPress(e: MotionEvent?) {
            var totlaPrice = 0.0
            (0..(bars - 1)).forEach { position ->
                val price = prices[position].value
                totlaPrice += (blocks.get(position, mutableListOf(0.0f)).sum() + building.consumptions[position]) * price
            }
            AlertDialog.Builder(context)
                    .setTitle("Daily prediction")
                    .setMessage("%.2f".format(totlaPrice) + " €")
                    .create().show()
        }
    })

    init {
        setLayerType(LAYER_TYPE_SOFTWARE, null)
        setOnTouchListener { _, motionEvent ->
            clickDetector.onTouchEvent(motionEvent)
            true
        }
    }

    fun setServerPrices(prices: List<Price>) {
        this.prices = prices
        invalidate()
    }

    fun setPlacements(up: UserPlacement) {
        blocks.clear()
        up.placement.forEach {
            if (blocks[it.time_segment_id] == null) {
                blocks.put(it.time_segment_id, mutableListOf())
            }
            blocks[it.time_segment_id].apply {
                add(applianceRepository.find(it.appliance).consumption)
            }
        }
        Log.v("Graph", "blocks = $blocks")
        invalidate()
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        daytimeMult = w / bars.toFloat()
        profitMult = h / (40.0f)
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        path.reset()
        path.moveTo(0.0f, height.toFloat())
        drawPrices(canvas)

        (0..(bars - 1)).forEach {
            drawDayTimeValue(it)
            drawDaytimeDivider(canvas, it)
            drawBlocks(canvas, it, blocks.get(it, mutableListOf()))
        }

        canvas.drawPath(path, paint)
    }


    private fun drawDayTimeValue(position: Int) {
        val price = (prices.getOrNull(position)?.value ?: 0.0f) * 50f
        val h = height - (blocks.get(position, mutableListOf(0.0f)).sum() + building.consumptions[position]) * price * profitMult

        Log.v("Graph", "price = $price")
        path.quadTo(
                daytimeMult * (position + 0.5f), h,
                daytimeMult * (position + 1.0f), height.toFloat()
        )
    }

    private fun drawPrices(canvas: Canvas) {
        prices.forEachIndexed { index, price ->
            val priceShow = "%.2f".format(price.value)
            canvas.drawText("$priceShow", daytimeMult * (index + 0.5f), 32f, pricePaint) //€/kWh
        }
    }

    private fun drawDaytimeDivider(canvas: Canvas, position: Int) {
        canvas.drawLine(daytimeMult * position.toFloat(), 0.0f, daytimeMult * position.toFloat(), height.toFloat(), daytimeDividerPaint)
    }

    private fun drawBlocks(canvas: Canvas, position: Int, blocks: List<Float>) {
//        if (blocks.sum() > profitValues[position]) {
//        } else {
//            blockPain.color = context.resources.getColor(R.color.success)
//        }
        var offset = 4f

        // Draw base
        blockPain.color = context.resources.getColor(R.color.base)
        val it = building.consumptions[position] * profitMult
        canvas.drawRect(daytimeMult * position + 4f, height - offset - it, daytimeMult * (position + 1f) - 4f, height - offset, blockPain)
        offset += (4f + it)

        // Draw blocks
        blockPain.color = context.resources.getColor(R.color.danger)
        val blockHeights = blocks.map { it * profitMult }

        blockHeights.forEach {
            canvas.drawRect(daytimeMult * position + 4f, height - offset - it, daytimeMult * (position + 1f) - 4f, height - offset, blockPain)
            offset += (4f + it)
        }
    }
}
package space.upsilon.supline.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import io.reactivex.android.schedulers.AndroidSchedulers
import space.upsilon.supline.data.DaySetup
import space.upsilon.supline.data.remote.api
import space.upsilon.supline.data.remote.entry.Building
import space.upsilon.supline.data.remote.entry.UserPlacement
import space.upsilon.supline.ext.asRetrofitBody
import space.upsilon.supline.ui.common.BaseActivity
import space.upsilon.supline.ui.dialog.ApplianceSelectDialog

/**
 * Author: Andrey Khitryy
 * Email: andrey.khitryy@gmail.com
 */
class DayActivity : BaseActivity(), ApplianceSelectDialog.Listener {

    lateinit var view: PriceGraphView
    lateinit var setup: UserPlacement

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setup = UserPlacement(intent.getStringExtra("day"))

        view = PriceGraphView(this, intent.getSerializableExtra("building") as Building, {
            ApplianceSelectDialog.newInstance(setup, it).show(supportFragmentManager, "select")
        })

        setContentView(view)

        onUpdated(setup)

        api.getPricesForDay(setup.day)
                .observeOn(AndroidSchedulers.mainThread())
                .asRetrofitBody()
                .subscribe(view::setServerPrices, this::processError)
                .bind()
    }

    override fun onUpdated(daySetup: UserPlacement) {
        Log.v("Graph", "day = $daySetup")
        this.setup = daySetup
        view.setPlacements(daySetup)
    }

    companion object {
        fun getStartIntent(context: Context, day: String, building: Building) =
                Intent(context, DayActivity::class.java).apply {
                    putExtra("day", day)
                    putExtra("building", building)
                }
    }
}
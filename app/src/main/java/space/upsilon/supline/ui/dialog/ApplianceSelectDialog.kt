package space.upsilon.supline.ui.dialog

import android.app.Dialog
import android.os.Bundle
import android.support.v7.app.AlertDialog
import space.upsilon.supline.data.DaySetup
import space.upsilon.supline.data.applianceRepository
import space.upsilon.supline.data.remote.entry.Placement
import space.upsilon.supline.data.remote.entry.UserPlacement
import space.upsilon.supline.ui.common.BaseDialog

/**
 * Author: Andrey Khitryy
 * Email: andrey.khitryy@gmail.com
 */
class ApplianceSelectDialog : BaseDialog() {

    lateinit var setup: UserPlacement
    var position: Int = 0

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        setup = arguments.getSerializable("setup") as UserPlacement
        position = arguments.getInt("position", 0)

        val appls = applianceRepository.getAll()

        val names = appls.map { it.name }.toTypedArray()
        val selected = appls.map { n -> setup.placement.any { it.appliance == n.id && it.time_segment_id == position } }.toBooleanArray()

        return AlertDialog.Builder(context)
                .setPositiveButton("OK", { d, b ->
                    (activity as? Listener)?.let {
                        setup.placement.removeAll { it.time_segment_id == position }
                        setup.placement.addAll(appls.filterIndexed { index, appliance -> selected[index] }.map {
                            Placement(position, it.id)
                        })
                        it.onUpdated(setup)
                    }
                    d.dismiss()
                })
                .setMultiChoiceItems(names, selected, { dialogInterface, i, checked ->
                    selected[i] = checked
                }).create()
    }

    interface Listener {
        fun onUpdated(daySetup: UserPlacement)
    }

    companion object {
        fun newInstance(setup: UserPlacement, position: Int) = ApplianceSelectDialog().apply {
            arguments = Bundle().apply {
                putSerializable("setup", setup)
                putInt("position", position)
            }
        }
    }
}